--[[
 Copyright 2019 Konsulko Group

 author:Edi Feschiyan <edi.feschiyan@konsulko.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
--]]

_AFT.describe("double subscribe",
    function()
        local api = "weather"

        _AFT.assertVerbStatusSuccess(api, "subscribe",{value = api})
        _AFT.assertVerbStatusSuccess(api, "subscribe",{value = api})

    end)

_AFT.testVerbStatusError("testSubscribeWrongArg","weather","subscribe",{}, nil, nil)
_AFT.testVerbStatusError("testUnsubscribeWrongArg","weather","unsubscribe",{}, nil, nil)
_AFT.testVerbStatusError("testSubscribeWrongInvalidValue","weather","subscribe",{value="bogus"}, nil, nil)
_AFT.testVerbStatusError("testUnsubscribeWrongInvalidValue","weather","unsubscribe",{value="bogus"}, nil, nil)
_AFT.testVerbStatusSuccess('testCurrentweatherSuccess','weather','current_weather', {})
_AFT.testVerbStatusSuccess('testAPIKeySuccess','weather','api_key', {})
_AFT.testVerbStatusSuccess('testSubscribeSuccess','weather','subscribe', {value="weather"})
_AFT.testVerbStatusSuccess('testUnsubscribeSuccess','weather','unsubscribe', {value="weather"})
